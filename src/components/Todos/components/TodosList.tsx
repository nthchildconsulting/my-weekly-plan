import React, { useState, memo } from 'react';
import { makeStyles } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import TodosListItem from './TodosListItem';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import strings from 'utils/localization';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Card from 'components/Card/Card';
import SwipeableViews from 'react-swipeable-views';
import { useTodos } from '../store/hooks';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    [theme.breakpoints.up('md')]: {
      width: '50vw',
      paddingTop: 50,
      marginBottom: 70,
    },
    [theme.breakpoints.down('md')]: {
      width: '80vw',
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  list: {
    width: '100%',
  },
  item: {
    width: '100%',
  },
  AppBar: {
    width: '100%',
  },
  center: {
    textAlign: 'center',
  },
  fullHeight: {
    minHeight: '100vh',
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(4),
    },
  },
}));

interface Props {
  type: string;
}

const Todos: React.FC<Props> = memo((props) => {
  const { type } = props;
  const classes = useStyles();
  const todos: any = useTodos();
  const rows = todos[type];
  return (
    <div className={classes.fullHeight}>
      <Card>
        {rows.length === 0 && (
          <Typography className={classes.center}>{strings.noTodos}</Typography>
        )}
        <List id="todoList" className={classes.list}>
          {rows.map((todo: any) => {
            return (
              <div className={classes.item} key={todo.id}>
                <TodosListItem todo={todo} />
                <Divider />
              </div>
            );
          })}
        </List>
      </Card>
    </div>
  );
});

const TodosList: React.FC = () => {
  const classes = useStyles();
  const [tab, setTab] = useState(0);
  const matches = useMediaQuery((theme: any) => theme.breakpoints.down('sm'));
  return (
    <div className={classes.root}>
      <AppBar
        position={matches ? 'fixed' : 'static'}
        className={classes.AppBar}
      >
        <Tabs value={tab} onChange={(e, v) => setTab(v)}>
          <Tab label={strings.inbox} />
          <Tab label={strings.nextWeek} />
          <Tab label={strings.completed} />
          <Tab label={strings.all} />
        </Tabs>
      </AppBar>
      <SwipeableViews index={tab} onChangeIndex={(v) => setTab(v)}>
        <Todos type="todosInbox" />
        <Todos type="todosNextWeek" />
        <Todos type="todosCompleted" />
        <Todos type="todosAll" />
      </SwipeableViews>
    </div>
  );
};

export default TodosList;
