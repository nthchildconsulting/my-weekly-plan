import React, { useState, useEffect } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  TimePicker,
  DatePicker,
} from '@material-ui/pickers';
import strings from 'utils/localization';
import TextField from '@material-ui/core/TextField';
import { useStore } from '../store/Context';
import { useSnackbar } from 'notistack';

interface Props {
  open: any;
  setOpen: any;
  defaultTodo: any;
}

const TodoDialog: React.FC<Props> = ({ open, setOpen, defaultTodo }: Props) => {
  const { dispatch }: any = useStore();
  const defaultDate = moment()
    .add(1, 'hour')
    .format();
  const [dueAt, setDueAt] = useState<any>(defaultDate);
  const [todoText, setTodoText] = useState('');
  const { enqueueSnackbar } = useSnackbar();
  const onHandleSave = () => {
    if (defaultTodo) {
      defaultTodo.dueAt = moment(dueAt).format();
      defaultTodo.todo = todoText;
      dispatch({ type: 'changeTodo', todo: defaultTodo });
    } else {
      dispatch({ type: 'addTodo', todo: todoText, dueAt });
      setTodoText('');
    }
    enqueueSnackbar('You have successfully saved the todo');
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  };
  useEffect(() => {
    if (defaultTodo) {
      if (defaultTodo.dueAt) {
        setDueAt(defaultTodo.dueAt);
      }
      if (defaultTodo.todo) {
        setTodoText(defaultTodo.todo);
      }
    }
  }, [defaultTodo]);
  return (
    <Dialog open={open} onClose={() => setOpen(false)}>
      <DialogTitle>{strings.todoDialogText}</DialogTitle>
      <DialogContent>
        <div>
          <TextField
            value={todoText}
            onChange={(e: any) => setTodoText(e.target.value)}
            fullWidth
            placeholder="What will you do this week?"
          />
        </div>
        <div>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <DatePicker
              margin="normal"
              label={strings.datePicker}
              value={dueAt}
              onChange={setDueAt}
            />
            <TimePicker
              margin="normal"
              label={strings.timePicker}
              value={dueAt}
              onChange={setDueAt}
            />
          </MuiPickersUtilsProvider>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          onClick={() => {
            setOpen(false);
          }}
        >
          {strings.close}
        </Button>
        <Button
          variant="outlined"
          onClick={() => {
            onHandleSave();
            setOpen(false);
          }}
          disabled={!todoText.trim().length}
        >
          {strings.save}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default TodoDialog;
