import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import TodoDialog from './TodoDialog';

const useStyles = makeStyles(() => ({
  fab: {
    position: 'fixed',
    bottom: 20,
    right: 40,
    zIndex: 1001,
  },
}));

const TodosCreate: React.FC = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <>
      <TodoDialog defaultTodo={null} open={open} setOpen={setOpen} />
      <Fab
        color="secondary"
        aria-label="add"
        className={classes.fab}
        onClick={() => setOpen(true)}
      >
        <AddIcon />
      </Fab>
    </>
  );
};

export default TodosCreate;
