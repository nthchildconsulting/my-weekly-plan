import React, { useState } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import SnoozeIcon from '@material-ui/icons/Snooze';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import TodoDialog from './TodoDialog';
import { useTodos } from '../store/hooks';

const useStyles = makeStyles((theme: Theme) => ({
  listItem: {
    flex: 'none',
    width: '70%',
  },
  listItemSecondaryAction: {
    display: 'flex',
    width: '15%',
    [theme.breakpoints.down('sm')]: {
      width: '20%',
    },
  },
}));

interface Props {
  todo: any;
}

const TodosListItem: React.FC<Props> = props => {
  const { dispatch }: any = useTodos();
  const { todo } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <ListItem button>
      <FormControlLabel
        label=""
        control={
          <Checkbox
            checked={todo.completed}
            onChange={() => {
              dispatch({ type: 'toggleTodo', todo });
            }}
          />
        }
      />
      <ListItemText
        classes={{
          root: classes.listItem,
        }}
        primary={
          <Typography onClick={() => setOpen(true)}>
            {moment(todo.dueAt).format('ddd, MMM Do YYYY, h:mm a')}
          </Typography>
        }
        secondary={
          <Typography onClick={() => setOpen(true)}>{todo.todo}</Typography>
        }
      />
      <ListItemSecondaryAction
        classes={{
          root: classes.listItemSecondaryAction,
        }}
      >
        <IconButton
          aria-label="Snooze"
          onClick={() => {
            dispatch({ type: 'snoozeTodo', todo });
          }}
        >
          <SnoozeIcon />
        </IconButton>
        <IconButton
          aria-label="Delete"
          onClick={() => {
            dispatch({ type: 'removeTodo', todo });
          }}
        >
          <DeleteIcon />
        </IconButton>
        <TodoDialog open={open} setOpen={setOpen} defaultTodo={todo} />
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default TodosListItem;
