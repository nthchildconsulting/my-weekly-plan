import React from 'react';
import renderer from 'react-test-renderer';
import Todos from '../Todos';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render todos', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Todos />
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
