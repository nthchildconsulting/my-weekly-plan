import React from 'react';
import renderer from 'react-test-renderer';
import TodosList from '../components/TodosList';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render todos list', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <TodosList />
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
