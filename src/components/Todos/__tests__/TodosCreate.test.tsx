import React from 'react';
import renderer from 'react-test-renderer';
import TodosCreate from '../TodosCreate';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render todos create', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <TodosCreate />
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
