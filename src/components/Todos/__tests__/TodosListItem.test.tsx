import React from 'react';
import renderer from 'react-test-renderer';
import TodosListItem from '../TodosListItem';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render todos list item', () => {
  const todo: any = {
    dueAt: new Date(),
  };
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <TodosListItem todo={todo} />
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
