import React, { useReducer, useContext, useMemo } from 'react';
import moment from 'moment';

const Context = React.createContext([{}, () => {}]);

const initialState = {
  todos: [],
  loaded: false,
};

const endOfWeek = moment().endOf('week');
const endOfNextWeek = moment()
  .add(1, 'week')
  .endOf('week');

const reducer = (state, action) => {
  switch (action.type) {
    case 'addTodo': {
      const nextId =
        state.todos.length > 0 ? Math.max(...state.todos.map(t => t.id)) : 0;
      const newTodo = {
        id: nextId + 1,
        todo: action.todo.trim(),
        createdAt: moment().format(),
        dueAt: moment(action.dueAt).format(),
        completed: false,
        completedAt: null,
      };
      return { ...state, todos: [...state.todos, newTodo] };
    }
    case 'toggleTodo': {
      const { todo } = action;
      if (todo.completed) {
        todo.completed = false;
        todo.completedAt = null;
      } else {
        todo.completed = true;
        todo.completedAt = moment().format();
      }
      const newTodos = state.todos.map(t => {
        if (t.id === todo.id) {
          return todo;
        }
        return t;
      });
      return { ...state, todos: newTodos };
    }
    case 'removeTodo': {
      const newTodos = state.todos.filter(t => t.id !== action.todo.id);
      return { ...state, todos: newTodos };
    }
    case 'snoozeTodo':
      const { todo } = action;
      todo.dueAt = moment(todo.dueAt)
        .add(1, 'days')
        .format();
      const newTodos = state.todos.map(t => {
        if (t.id === todo.id) {
          return todo;
        }
        return t;
      });
      return { ...state, todos: newTodos };
    case 'changeTodo': {
      const { todo } = action;
      const newTodos = state.todos.map(t => {
        if (t.id === todo.id) {
          return todo;
        }
        return t;
      });
      return { ...state, todos: newTodos };
    }
    case 'loadTodos':
      return { ...state, todos: action.todos, loaded: true };
    case 'reset':
      return { ...initialState };
    default:
      throw new Error('Unexpected action');
  }
};

const Provider = props => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const todosNextWeek = useMemo(() => {
    return state.todos
      .filter(
        todo =>
          !todo.completed &&
          moment(todo.dueAt).isAfter(endOfWeek) &&
          moment(todo.dueAt).isBefore(endOfNextWeek)
      )
      .sort(
        (a, b) =>
          Number(moment(a.dueAt).format('X')) -
          Number(moment(b.dueAt).format('X'))
      );
  }, [state.todos]);

  const todosAll = useMemo(() => {
    return state.todos.sort(
      (a, b) =>
        Number(moment(b.dueAt).format('X')) -
        Number(moment(a.dueAt).format('X'))
    );
  }, [state.todos]);

  const todosCompleted = useMemo(() => {
    return state.todos
      .filter(todo => todo.completed)
      .sort(
        (a, b) =>
          Number(moment(b.dueAt).format('X')) -
          Number(moment(a.dueAt).format('X'))
      );
  }, [state.todos]);

  const todosInbox = useMemo(() => {
    return state.todos
      .filter(todo => !todo.completed && moment(todo.dueAt).isAfter(moment()))
      .filter(todo => moment(todo.dueAt).isBefore(endOfWeek))
      .sort(
        (a, b) =>
          Number(moment(a.dueAt).format('X')) -
          Number(moment(b.dueAt).format('X'))
      );
  }, [state.todos]);

  return (
    <Context.Provider
      value={{
        state,
        dispatch,
        todosAll,
        todosNextWeek,
        todosCompleted,
        todosInbox,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export const useStore = () => useContext(Context);

export function withProvider(Component) {
  return function WrapperComponent(props) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };
