import { useCallback } from 'react';
import { useStore } from './Context';
import * as Firebase from 'services/Firebase';
import { useApp } from 'components/App/store/hooks';

export const useTodos = () => {
  const { user } = useApp();

  const {
    state,
    dispatch,
    todosAll,
    todosNextWeek,
    todosCompleted,
    todosInbox,
  } = useStore();

  const { loaded, todos } = state;

  const fetchTodos = useCallback(async () => {
    if (user && user.uid) {
      const response = await Firebase.getTodos(user.uid);
      dispatch({ type: 'loadTodos', todos: response });
    }
  }, [dispatch, user]);

  // Setup a listener to when a new todo is added
  const listenForTodos = useCallback(() => {
    if (user && user.uid && !loaded) {
      let isListening = false;
      Firebase.listenForTodo(user.uid, () => {
        if (!isListening) {
          setTimeout(() => fetchTodos(), 2000);
          isListening = true;
        } else {
          fetchTodos();
        }
      });
    }
  }, [fetchTodos, loaded, user]);

  // If the todos ever change then save them to firebase
  const saveTodos = useCallback(() => {
    if (user && user.uid && loaded && todos) {
      Firebase.saveTodos(user.uid, todos);
    }
  }, [loaded, todos, user]);

  return {
    ...state,
    dispatch,
    todosAll,
    todosNextWeek,
    todosCompleted,
    todosInbox,
    fetchTodos,
    saveTodos,
    listenForTodos,
  };
};
