import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import TodosCreate from './components/TodosCreate';
import TodosList from './components/TodosList';
import { withProvider } from './store/Context';
import { useTodos } from './store/hooks';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
}));

const Todos = () => {
  const classes = useStyles();
  const { loaded, listenForTodos, saveTodos }: any = useTodos();

  useEffect(() => {
    listenForTodos();
  }, [listenForTodos]);

  useEffect(() => {
    saveTodos();
  }, [saveTodos]);

  if (!loaded) {
    return (
      <div className={classes.root} style={{ height: '100%' }}>
        <CircularProgress />
      </div>
    );
  }
  return (
    <div className={classes.root}>
      <TodosCreate />
      <TodosList />
    </div>
  );
};

export default withProvider(Todos);
