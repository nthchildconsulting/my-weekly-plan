import React from 'react';
import Todos from 'components/Todos/Todos';
import RightDrawer from 'components/Drawer/RightDrawer';
import Login from 'components/Login/Login';
import { withProvider } from './store/Context';
import { useApp, withThemeProvider } from './store/hooks';
import { SnackbarProvider } from 'notistack';

const App: React.FC = () => {
  const { user, showLogin }: any = useApp();

  if (!user && !showLogin) {
    return <></>;
  }

  if (!user) {
    return <Login />;
  }

  return (
    <SnackbarProvider>
      <>
        <Todos />
        <RightDrawer />
      </>
    </SnackbarProvider>
  );
};

export default withProvider(withThemeProvider(App));
