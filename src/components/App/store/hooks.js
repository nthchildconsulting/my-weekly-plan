import React, { useCallback, useEffect } from 'react';
import { useStore } from './Context';
import * as Firebase from 'services/Firebase';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import defaultTheme from 'themes/default';
import darkTheme from 'themes/dark';

require('moment/locale/ja.js');

export const useApp = () => {
  const {
    state,
    dispatch,
    language,
    changeLanguage,
    darkMode,
    setDarkMode,
  } = useStore();

  const authListener = useCallback(() => {
    Firebase.listenForAuth(user => {
      if (!user) {
        dispatch({ type: 'showLogin' });
      }
      if (!state.user && user) {
        dispatch({ type: 'login', user });
      }
    });
    return () => {
      // Cleanup
    };
  }, [dispatch, state.user]);

  useEffect(() => {
    const unsubscribe = authListener();
    return () => {
      unsubscribe();
    };
  }, [authListener]);

  return {
    ...state,
    dispatch,
    language,
    changeLanguage,
    darkMode,
    setDarkMode,
  };
};

export function withThemeProvider(Component) {
  return function WrapperComponent(props) {
    const { darkMode } = useApp();
    const theme = darkMode ? darkTheme : defaultTheme;
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </ThemeProvider>
    );
  };
}
