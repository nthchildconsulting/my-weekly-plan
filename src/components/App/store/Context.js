import React, { useReducer, useContext } from 'react';
import useLocalStorage from 'hooks/useLocalStorage';
import moment from 'moment';
import strings from 'utils/localization';

const Context = React.createContext([{}, () => {}]);

const initialState = {
  user: null,
  showLogin: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'showLogin':
      return { ...state, showLogin: true };
    case 'login':
      return { ...state, user: action.user, showLogin: false };
    case 'logout':
      return { ...state, user: null, showLogin: true };
    case 'reset':
      return { ...initialState };
    default:
      throw new Error('Unexpected action');
  }
};

const Provider = props => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [darkMode, setDarkMode] = useLocalStorage('darkMode', false);
  const [language, setLanguage] = useLocalStorage('language', 'en');

  const changeLanguage = lang => {
    setLanguage(lang);
  };

  strings.setLanguage(language);
  moment.locale(language);
  return (
    <Context.Provider
      value={{
        state,
        dispatch,
        language,
        changeLanguage,
        darkMode,
        setDarkMode,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export const useStore = () => useContext(Context);

export function withProvider(Component) {
  return function WrapperComponent(props) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };
