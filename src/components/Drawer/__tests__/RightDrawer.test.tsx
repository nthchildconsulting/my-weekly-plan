import React from 'react';
import renderer from 'react-test-renderer';
import RightDrawer from '../RightDrawer';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render right drawer', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <RightDrawer>Test</RightDrawer>
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
