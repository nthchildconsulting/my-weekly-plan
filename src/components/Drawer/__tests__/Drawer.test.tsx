import React from 'react';
import renderer from 'react-test-renderer';
import Drawer from '../Drawer';
import AppsIcon from '@material-ui/icons/Apps';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render drawer', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Drawer icon={AppsIcon}>Test</Drawer>
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
