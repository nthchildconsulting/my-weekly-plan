import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'fixed',
    top: -5,
    right: 20,
    zIndex: 9001,
  },
  drawer: {
    width: 350,
    padding: theme.spacing(),
  },
}));

interface Props {
  children: React.ReactNode;
  icon: any;
}

const Drawer: React.FC<Props> = ({ children, icon: MenuIcon }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <div className={classes.root}>
      <IconButton
        color="inherit"
        aria-label="Settings"
        onClick={() => setOpen(!open)}
      >
        <Typography>
          <MenuIcon fontSize="large" />
        </Typography>
      </IconButton>
      <SwipeableDrawer
        anchor={'right'}
        open={open}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
      >
        <div className={classes.drawer}>{children}</div>
      </SwipeableDrawer>
    </div>
  );
};

export default Drawer;
