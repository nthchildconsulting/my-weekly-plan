import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Drawer from 'components/Drawer/Drawer';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Divider from '@material-ui/core/Divider';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import strings from 'utils/localization';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import * as Firebase from 'services/Firebase';
import { useApp } from 'components/App/store/hooks';

const useStyles = makeStyles((theme: Theme) => ({
  item: {
    margin: theme.spacing(2),
  },
}));

const RightDrawer: React.FC = () => {
  const classes = useStyles();
  const {
    darkMode,
    setDarkMode,
    language,
    changeLanguage,
    user,
  }: any = useApp();
  const isLoggedIn = user && user.uid;
  const logout = async () => {
    await Firebase.logout();
    window.location.href = '/';
  };
  const clearCompletedTasks = async () => {
    if (isLoggedIn) {
      await Firebase.clearCompletedTasks(user.uid);
    }
  };
  return (
    <Drawer icon={MenuIcon}>
      <Typography
        component="h5"
        variant="h6"
        align="center"
        style={{
          margin: 10,
        }}
      >
        {strings.settings}
      </Typography>
      <Divider />
      <div className={classes.item}>
        <FormControlLabel
          control={
            <Switch
              checked={darkMode}
              onChange={() => setDarkMode(!darkMode)}
            />
          }
          label={strings.darkMode}
        />
      </div>
      <Divider />
      <div className={classes.item}>
        <FormControl>
          <InputLabel htmlFor="language">{strings.language}</InputLabel>
          <Select
            value={language}
            onChange={e => changeLanguage(e.target.value)}
            input={<Input name="language" id="language" />}
          >
            <MenuItem value={'en'}>English</MenuItem>
            <MenuItem value={'ja'}>日本語</MenuItem>
          </Select>
        </FormControl>
      </div>
      <Divider />
      {isLoggedIn && (
        <div className={classes.item}>
          <Button
            variant="contained"
            color="primary"
            onClick={clearCompletedTasks}
          >
            Clear Completed Tasks
          </Button>
        </div>
      )}
      <div className={classes.item}>
        <Button variant="contained" color="primary" onClick={logout}>
          Logout
        </Button>
      </div>
    </Drawer>
  );
};

export default RightDrawer;
