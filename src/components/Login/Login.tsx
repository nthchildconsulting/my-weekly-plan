import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import logo from 'assets/img/logo.png';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import * as Firebase from 'services/Firebase';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    height: '100%',
  },
  paper: {
    width: 400,
    height: 300,
  },
  header: {
    textAlign: 'center',
    margin: theme.spacing(),
  },
  paragraph: {
    textAlign: 'center',
    margin: theme.spacing(),
  },
  button: {
    textAlign: 'center',
    margin: theme.spacing(),
  },
  logo: {
    width: 50,
  },
}));

const Login: React.FC = () => {
  const classes = useStyles();
  const login = () => {
    Firebase.login();
  };
  return (
    <Grid
      container
      justify="center"
      alignContent="center"
      className={classes.root}
    >
      <Grid>
        <Paper className={classes.paper}>
          <Grid
            container
            justify="center"
            alignContent="center"
            className={classes.root}
            direction="column"
          >
            <div className={classes.header}>
              <img src={logo} alt="logo" className={classes.logo} />
              <h1>My Weekly Plan</h1>
            </div>
            <div className={classes.paragraph}>You are not logged in.</div>
            <div className={classes.button}>
              <Button variant="contained" color="primary" onClick={login}>
                Google Login
              </Button>
            </div>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default Login;
