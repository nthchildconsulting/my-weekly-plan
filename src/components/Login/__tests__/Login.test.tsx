import React from 'react';
import renderer from 'react-test-renderer';
import Login from '../Login';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render login', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Login />
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
