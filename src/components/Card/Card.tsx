import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    minWidth: 350,
    marginBottom: theme.spacing(),
  },
  title: {
    fontSize: 14,
  },
}));

interface Props {
  title?: string;
  children?: React.ReactNode;
  actions?: React.ReactNode;
}

const SimpleCard: React.FC<Props> = props => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} component="h1">
          {props.title}
        </Typography>
        {props.children}
      </CardContent>
      <CardActions>{props.actions}</CardActions>
    </Card>
  );
};

export default SimpleCard;
