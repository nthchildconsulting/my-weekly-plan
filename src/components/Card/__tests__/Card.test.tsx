import React from 'react';
import renderer from 'react-test-renderer';
import Card from '../Card';
import { ThemeProvider } from '@material-ui/styles';
import theme from 'themes/default';

it('will render card', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Card>Test</Card>
      </ThemeProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
