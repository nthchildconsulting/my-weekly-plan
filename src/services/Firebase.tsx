import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const app = firebase.initializeApp({
  apiKey: 'AIzaSyD1wqw5vPIFgyPw_kyhF_R4BvD4-MkOrVA',
  authDomain: 'my-weekly-plan.firebaseapp.com',
  databaseURL: 'https://my-weekly-plan.firebaseio.com',
  projectId: 'my-weekly-plan',
  storageBucket: 'my-weekly-plan.appspot.com',
  messagingSenderId: '147004132223',
});

const db = firebase.firestore(app);

const login = () => {
  const provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithRedirect(provider);
};

const logout = async () => {
  await firebase.auth().signOut();
};

const listenForAuth = (cb: any) => {
  firebase.auth().onAuthStateChanged(cb);
};

const listenForTodo = (id: string, cb: any) => {
  let doc = db.collection('users').doc(id);
  doc.onSnapshot(cb);
};

const getTodos = async (id: string) => {
  const doc = await db
    .collection('users')
    .doc(id)
    .get();
  const data: any = doc.data();
  const response = data && data.todos ? data.todos : [];
  return response;
};

function saveTodos(id: string, data: any) {
  const options = {
    todos: data,
  };
  return db
    .collection('users')
    .doc(id)
    .set(options);
}

const clearCompletedTasks = async (id: string) => {
  const todos = await getTodos(id);
  const newTodos = todos.filter((todo: any) => !todo.completed);
  return saveTodos(id, newTodos);
};

export {
  login,
  logout,
  listenForAuth,
  getTodos,
  saveTodos,
  listenForTodo,
  clearCompletedTasks,
};
